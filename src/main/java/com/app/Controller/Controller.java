package com.app.Controller;

import com.app.Domain.User;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller
{
    @Autowired
    Receiver receiver;

    @Autowired
    Sender sender;

    @GetMapping("/user")
    private void receiveRequest()
    {
        receiver.receive("r2c ");
    }

    @PostMapping("/user")
    private int saveStudent(@RequestBody User user)
    {
        sender.send(""+
                user.getId()+"-"+
                user.getCode()+"-"+
                user.getName()+"-"+
                user.getEmail());
        return 0;
    }
}
