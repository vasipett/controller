package com.app.Sender;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private Queue queue;

    public void send(String userMessage) {
        this.template.convertAndSend(queue.getName(), userMessage);
        System.out.println(" [x] Sent '" + userMessage + "'");
    }
}
