package com.app.Domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class User
{
    //mark id as primary key
    @Id
    //defining id as column name
    @Column
    private int id;
    //defining name as column name
    @Column
    private String name;
    //defining age as column name
    @Column
    private int code;
    //defining email as column name
    @Column
    private String email;

    public int getId()
    {
        return id;
    }
    public String getName()
    {
        return name;
    }
    public int getCode()
    {
        return code;
    }
    public String getEmail()
    {
        return email;
    }
}